package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world",translator.getPhrase());
	}

	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals("nil",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartWithVowelEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartWithConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartWithMoreConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMorePhrases() {
		String inputPhrase = "hello world";
		String inputPhrase2 = "well-being";
		
		Translator translator = new Translator(inputPhrase);
		Translator translator2 = new Translator(inputPhrase2);
		
		assertEquals("ellway-eingbay",translator2.translate());
		assertEquals("ellohay orldway",translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseWithPunctuations() {
		String inputPhrase = "hello world!";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway!",translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseInUpperCase() {
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		assertEquals("APPLEYAY",translator.translate());
		
	}
	
	@Test
	public void testTranslationTitleCase() {
		String inputPhrase = "Hello";
		
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("Ellohay",translator.translate());
	}
}
