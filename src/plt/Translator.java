package plt;

import java.util.ArrayList;
import java.util.List;


public class Translator {
	
	public enum CharType{
		SPACE,HYPHEN,LETTER,PUNCT
	}
	
	private String phrase;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}
	
	public String getPhrase() {
		return phrase;
	}
	
	public String translate() {
		if(phrase.isBlank()) {
			return "nil";
		}
		
		List<String> tokens = getTokens();
		
		StringBuilder sb = new StringBuilder();
		
		for(String token : tokens) {
			sb.append(translateSingle(token));
		}
		
		return sb.toString();
	}
	
	private List<String> getTokens() {
		List<String> tokens = new ArrayList<>();
		StringBuilder buf = new StringBuilder();
		buf.append(phrase.charAt(0));
		CharType type = getType(phrase.charAt(0));
		
		for(int i=1; i<phrase.length(); i++) {
			char curCh = phrase.charAt(i);
			CharType curType = getType(curCh);
			
			if(curType.equals(type)) {
				buf.append(curCh);
			} else {
				tokens.add(buf.toString());
				
				buf.setLength(0);
				buf.append(curCh);
				type = curType;
			}
		}
		tokens.add(buf.toString());
		return tokens;
	}
	
	private CharType getType(char c) {
		if(isHyphen(c)) {
			return CharType.HYPHEN;
		} else if(isSpace(c)) {
			return CharType.SPACE;
		} else if (isPunct(c)) {
			return CharType.PUNCT;
		} else if (Character.isAlphabetic(c)) {
			return CharType.LETTER;
		} else {
			return null;
		}
	}
	
	private boolean isSpace(char c) {
		return ' ' == c;
	}
	
	private boolean isHyphen(char c) {
		return '-' == c;
	}

	private boolean isPunct(char c) {
		return '.' == c || ',' == c || ';' == c || '?' == c || '!' == c || '('== c || ')' == c;
	}
	
	private String translateSingle(String token) {
		String translated = translateToken(token);
		
		if(isUpperCase(token)) {
			return translated.toUpperCase();
		} else if (isTitle(token)) {
			return translated.substring(0,1).toUpperCase() + translated.substring(1).toLowerCase();
		} else {
			return translated;
		}
	}
	
	private boolean isTitle(String token) {
		return Character.isUpperCase(token.charAt(0));
	}
	
	private boolean isUpperCase(String token) {
		return token.toUpperCase().equals(token);
	}
	
	private String translateToken(String token) {
		if(startWithVowel(token)) {
			if(token.endsWith("y")) {
				return token + "nay";
			}
			if(endWithVowel(token)) {
				return token + "yay";
			}
			if(endWithConsonant(token)) {
				return token + "ay";
			}
		}
		
		if(startWithConsonant(token)) {
			int vowelPos = findFirstVowel(token);
			return token.substring(vowelPos) + token.substring(0,vowelPos) + "ay";
		}
		return token;
	}
	
	private int findFirstVowel(String phrase) {
		for(int i=0; i<phrase.length(); i++) {
			char ch = phrase.charAt(i);
			
			if(isVowel(ch)) {
				return i;
			}
		}
		return -1;
	}
	
	private boolean startWithConsonant(String s) {
		char firstCh = s.charAt(0);
		return isConsonant(firstCh);
	}
	
	private boolean endWithConsonant(String s) {
		char lastCh = s.charAt(s.length()-1);
		return isConsonant(lastCh);
	}
	
	private boolean isConsonant(char c) {
		return Character.isAlphabetic(c) && !isVowel(c);
	}
	
	private boolean startWithVowel(String s) {
		char firstCh = s.charAt(0);
		return isVowel(firstCh);
	}
	
	private boolean endWithVowel(String s) {
		char lastCh = s.charAt(s.length()-1);
		return isVowel(lastCh);
	}
	
	private boolean isVowel(char c) {
		char e = Character.toLowerCase(c);
		return 'a' == e || 'e'==e || 'i'==e || 'o'==e || 'u'==e ;
	}
}